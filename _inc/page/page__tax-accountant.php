<section class="sec03">
    <h1 class="sec03__title wow animate__animated animate__fadeInUp" data-wow-duration="1s">豊富な法律知識と実績から 最善の解決方法を ご提案します</h1>
    <p class="sec03__description wow animate__animated animate__fadeInUp" data-wow-duration="1s">税理士は各種税金・税務申告に関する問題をはじめ、<br class="sec03__sp">
    会社の経理や経営相談、相続に代表されるご家族の悩みなどを相談できる専門家です。<br class="sec03__sp">
    相続問題、独立・起業をお考えの方へのサポート、法人・個人事業主様の税務申告・<br class="sec03__sp">
    会計業務と経営支援を業務の中心として取り組んでおります。<br class="sec03__sp">
    フットワーク軽く、一緒に考え、一緒に悩み、皆様の不安を解決して参ります。<br class="sec03__sp">
    一人で悩まず、まずはご相談下さい。
    </p>
</section>