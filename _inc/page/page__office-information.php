<section class="sec08"> 
    <h3 class="sec08__text">アクセス</h3>
    <div class="sec08_mapContainer wow animate__animated animate__zoomIn" data-wow-duration="1.5s">
    <iframe class="sec08__map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6479.87846238604!2d139.580161!3d35.703113!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd46a7a747391afaf!2z5ZCJ56Wl5a-66aeF!5e0!3m2!1sja!2sph!4v1617862231273!5m2!1sja!2sph" width="883" height="440" allowfullscreen="" loading="lazy"> </iframe>
    </div>
    <p class="sec08__description">JR線「吉祥寺駅」から徒歩7分</p>
</section>