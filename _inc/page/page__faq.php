<section class="faqSec02">
  <div class="faqSec02__container">
    <ul class="faqSec02__breadcrumb"> 
      <li class="faqSec02__breadcrumbItem"><a class="faqSec02__breadcrumbLink" href="<?php echo home_url() ?>">トップ</a></li>
      <li class="faqSec02_breadcrumbItem">よくある質問</li>
    </ul>
    <div class="faqSec02__btns"><a class="faqSec02__btn wow animate__animated animate__zoomIn" data-wow-duration="1.5s" href="#faqList-01">相続税について</a><a class="faqSec02__btn wow animate__animated animate__zoomIn" data-wow-duration="1.5s" data-wow-delay=".5s" href="#faqList-02">顧問契約について</a></div>
  </div>
</section>
<section class="faqSec03">
  <div class="faqSec03__container">
    <div class="faqSec03__faq" id="faqList-01">
      <h1 class="faqSec03__title">相続税について</h1>
      <ul class="faqSec03__qaList">
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s">
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキストテキストキストテキストテキストテキストテキスト。<br>テキストテキストテキストテキストテテキストテキストキスト。<br>テキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキスト。<br>テキストテキストテキストテキストテテキストテキストテキストキストテキストテキストテキストテキストキスト。<br>テキストテキストテキストテキステキストテキストテキストテキスト。  （質問への回答）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテキストテキストテキストテキストテキスト。<br>テキストテキストテキストテキストテキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキスト。テキストテキストテキストテキストテテキストテキストテキスト<br>テキストテキストキストテキストテキストテキストテキストキスト。<br>テキストテキストテキストテキステキストテキストテキステキストテキストトテキスト。  （質問への回答）</p>
        </li>
      </ul>
    </div>
    <div class="faqSec03__faq" id="faqList-02">
      <h1 class="faqSec03__title">相続税について</h1>
      <ul class="faqSec03__qaList">
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s">
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキストテキストキストテキストテキストテキストテキスト。<br>テキストテキストテキストテキストテテキストテキストキスト。<br>テキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキスト。<br>テキストテキストテキストテキストテテキストテキストテキストキストテキストテキストテキストテキストキスト。<br>テキストテキストテキストテキステキストテキストテキストテキスト。  （質問への回答）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテキストテキストテキストテキストテキスト。<br>テキストテキストテキストテキストテキストテキストテキストテキストテキステキストテキストテキストテキスト。 （よくある質問）</p>
        </li>
        <li class="faqSec03__qaItem wow animate__animated animate__fadeInUp" data-wow-duration="1s"> 
          <p class="faqSec03__qaItemText">原稿はダミーです、テキストテキストテキストテテキスト。テキストテキストテキストテキストテテキストテキストテキスト<br>テキストテキストキストテキストテキストテキストテキストキスト。<br>テキストテキストテキストテキステキストテキストテキステキストテキストトテキスト。  （質問への回答）</p>
        </li>
      </ul>
    </div>
  </div>
</section>