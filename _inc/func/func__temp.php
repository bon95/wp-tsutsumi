<?php
/*--------------------------------------------------------------
wp_head Setting (Include CSS/JS files + Add feed)
--------------------------------------------------------------*/
// Include CSS/JS files
function my_wp_head() {
    // CSS (in <head>)
    wp_enqueue_style( 'swiper', get_template_directory_uri() . '/_assets/js/lib/swiper/swiper.css', '', '4.4.2' );
    wp_enqueue_style( 'style', get_template_directory_uri() . '/_assets/css/style.css', '', '1.0' );
    wp_enqueue_style( 'animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css', '', '4.1.1' );

    // JS (before </body>)
    wp_deregister_script('jquery'); // Don't include WP Default jquery files
    wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js', '', '3.3.1', true );
    wp_enqueue_script( 'wow', 'https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js', '', '1.1.2', true );
    wp_enqueue_script( 'swiper', get_template_directory_uri() . '/_assets/js/lib/swiper/swiper.js', '', '4.4.2', true );
    wp_enqueue_script( 'script', get_template_directory_uri() . '/_assets/js/bundle.js', '', '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'my_wp_head' );

// Add feed
add_theme_support( 'automatic-feed-links' );

function my_mwform_enqueue_scripts() {
    wp_enqueue_script( 'mwform-script', get_template_directory_uri() . '/js/mwform.js', array(), '', true );
}
add_action( 'mwform_enqueue_scripts_mw-wp-form-118', 'my_mwform_enqueue_scripts' );


// my custom menus
function register_my_menus() {
    register_nav_menus(
        array(
        'header-menu' => __( 'Primary Menu' ),
        'extra-menu' => __( 'Extra Menu' )
        )
    );
}
add_action( 'init', 'register_my_menus' );

// add class for li items
function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

// add class for anchor tag
function add_additional_class_on_a($classes, $item, $args)
{
    if (isset($args->add_a_class)) {
        $classes['class'] = $args->add_a_class;
    }
    return $classes;
}
add_filter('nav_menu_link_attributes', 'add_additional_class_on_a', 1, 3);

/*--------------------------------------------------------------
Check New date (for post)
--------------------------------------------------------------*/
function check_new_date( $setted_new_days = 7, $entry_date ) {
    // $setted_new_days: date of new post
    // $entry_date: post date
    $now_date = date_i18n('U');  // now time
    $passed_days = date('U', ($now_date - $entry_date)) / 86400; // now - entry

    if ( $passed_days <= $setted_new_days ):
        return true; // new
    else:
        return false; // old
    endif;
}


/*--------------------------------------------------------------
Output <title>
--------------------------------------------------------------*/
function tsutsumi_theme_slug_setup() {
    add_theme_support( 'title-tag' );
    add_theme_support('custom-logo');
}
add_action( 'after_setup_theme', 'tsutsumi_theme_slug_setup' );


// Title Separator
function change_title_separator( $sep ){
    $sep = ' | ';
    return $sep;
}
add_filter( 'document_title_separator', 'change_title_separator' );


/*--------------------------------------------------------------
Path to uploads directory Function and Short code
--------------------------------------------------------------*/
function uploads_path() {
    $upload_dir = wp_upload_dir(); // https://domain.com/WordPress/wp-content/uploads/
    return $upload_dir['baseurl'];
}
add_shortcode('uploads_path', 'uploads_path'); // [uploads_path]


/*--------------------------------------------------------------
Slug → Object (Pages)
--------------------------------------------------------------*/
function pageInfo($slug, $info) {
    $pages = get_page_by_path($slug);
    return $pages->$info;
}


/*--------------------------------------------------------------
No Image Function
--------------------------------------------------------------*/
function noimage($url) {
    if ( $url == "" ):
        $url = get_template_directory_uri().'/_assets/images/_etc/noimage.jpg'; // No Image File
    endif;

    return $url;
}


/*-----------------------------------------------------------
Custom WP Page Navi's Pagination
------------------------------------------------------------*/
function custom_wp_pagenavi($html) {
    $out = '';

    $out = str_replace("<div class='wp-pagenavi'>", "", $html);
    $out = str_replace("</div>", "", $out);
    return '<div class="u-pager__list">'.$out.'</div>';
}
// add_filter( 'wp_pagenavi', 'custom_wp_pagenavi' );


/*--------------------------------------------------------------
Output Excerpt of Post title
--------------------------------------------------------------*/
function return_title( $content, $length ) {
    global $post;
    $text_length = $length;

    $content = strip_tags( $post->post_title );
    $content = strip_shortcodes( $content );
    if( mb_strlen( $content, "utf-8") > $text_length ):
        $title = mb_substr( $content, 0, $text_length, "utf-8" );
        return $title. '...';
    else:
        return $content;
    endif;
}


/*--------------------------------------------------------------
Output Excerpt of Post contents
--------------------------------------------------------------*/
function return_content( $content, $length ) {
    global $post;
    $text_length = $length;

    $content =  preg_replace( '/<!--more-->.+/is',"",$content ); // Remove contents after <more>
    $content =  strip_shortcodes( $content ); // Remove Short Code
    $content =  strip_tags( $content ); // Remove Tag
    $content =  str_replace( "&nbsp;", "", $content ); // Remove Space
    $content = strip_shortcodes( $content );
    if( mb_strlen( $content, "utf-8" ) > $text_length ):
        $title = mb_substr( $content, 0, $text_length, "utf-8" );
        return $title. '...';
    else:
        return $content;
    endif;
}



/*--------------------------------------------------------------
Remove auto generate <p> from Post the_content
--------------------------------------------------------------*/
add_filter('the_content', 'wpautop_filter', 9);
function wpautop_filter($content) {
    global $post;

    $arr_types = ['page', 'works_project'];
    $post_type = get_post_type( $post->ID );

    if ($post_type !== 'news'): // NEWS以外
        remove_filter('the_content', 'wpautop');
        remove_filter('the_excerpt', 'wpautop');
    endif;

    return $content;
}


/*--------------------------------------------------------------
Output Thumbnail
$size = 出力したいサムネイルの種類名
$num = 出力形式 '0'->url, '1'->width, '2'->height
--------------------------------------------------------------*/
function opt_thumb_data($size, $noimg, $num) {
    $thumbnail_id = get_post_thumbnail_id($post->ID);
    if ( $size == '' ):
        $size = 'full';
    endif;

    if ( $thumbnail_id ):
        $image = wp_get_attachment_image_src( $thumbnail_id, $size );
        if ( $num == '' ):
            echo $image[0];
        else:
            echo $image[num];
        endif;
    else:
        if ( $noimg == '' ):
            echo get_template_directory_uri() . '/_assets/images/_etc/noimage.jpg';
        else:
            echo get_template_directory_uri() . '/_assets/images/_etc/' . $noimg;
        endif;
    endif;
}


/*--------------------------------------------------------------
Output alt of thumbnail
--------------------------------------------------------------*/
function thumb_alt() {
    if ( has_post_thumbnail() ):
        return the_title();
    else:
        return 'noimage';
    endif;
}


/*--------------------------------------------------------------
Empty Keyword Search
--------------------------------------------------------------*/
function search_no_keywords() {
    if (isset($_GET['s']) && empty($_GET['s'])):
        include(TEMPLATEPATH . '/search.php');
        exit;
    endif;
}
add_action('template_redirect', 'search_no_keywords');


/*--------------------------------------------------------------
Remove unnecessary wp_head
--------------------------------------------------------------*/
remove_action('wp_head', 'feed_links_extra',3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'rest_output_link_wp_head');
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');


/*--------------------------------------------------------------
Remove Emoji
--------------------------------------------------------------*/
remove_action('wp_head', 'print_emoji_detection_script',7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
remove_filter('the_content_feed', 'wp_staticize_emoji');
remove_filter('comment_text_rss', 'wp_staticize_emoji');
remove_filter('wp_mail', 'wp_staticize_emoji_for_email');


/*--------------------------------------------------------------
Redirect Author Page → Top Page (for Security)
--------------------------------------------------------------*/
function theme_slug_redirect_author_archive() {
    if (is_author() ) {
        wp_redirect( home_url());
        exit;
    }
}
add_action( 'template_redirect', 'theme_slug_redirect_author_archive' );


/*--------------------------------------------------------------
Hide Rest API Information (for Security)
--------------------------------------------------------------*/
function my_filter_rest_endpoints( $endpoints ) {
    if ( isset( $endpoints['/wp/v2/users'] ) ) {
        unset( $endpoints['/wp/v2/users'] );
    }
    if ( isset( $endpoints['/wp/v2/users/(?P[\d]+)'] ) ) {
        unset( $endpoints['/wp/v2/users/(?P[\d]+)'] );
    }
    return $endpoints;
}
add_filter( 'rest_endpoints', 'my_filter_rest_endpoints', 10, 1 );
?>
