<ul class="postListA">
    <li class="postListA__item">
        <time class="postListA__time" datetime="2019-01-01">2019/01/01</time>
        <a href="#" class="postListA__catLabel" style="color: #00FF88; border-color: #00FF88;">カテゴリー名</a>
        <a href="#" class="postListA__postLink">投稿タイトルが入ります。投稿タイトルが入ります。</a>
    </li>
    <li class="postListA__item">
        <time class="postListA__time" datetime="2019-01-01">2019/01/01</time>
        <a href="#" class="postListA__catLabel" style="color: #EE00FF; border-color: #EE00FF;">カテゴリー名</a>
        <a href="#" class="postListA__postLink">投稿タイトルが入ります。</a>
    </li>
    <li class="postListA__item">
        <time class="postListA__time" datetime="2019-01-01">2019/01/01</time>
        <a href="#" class="postListA__catLabel" style="color: #F8F86F; border-color: #F8F86F;">カテゴリー名</a>
        <a href="#" class="postListA__postLink">投稿タイトルが入ります。</a>
    </li>
</ul>
