<?php get_header(); ?>

  <section class="sec01">
      <div class="swiper-container">
        <div class="swiper-wrapper">
          <img class="swiper-slide" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec01_item-01.png" alt="businessman">
          <img class="swiper-slide" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec01_item-01.png" alt="businessman">
        </div>
      </div>
    </section>
    
    
    <section class="sec02">
      <ul class="sec02__list"> 
        <li class="sec02__listItem wow animate__animated animate__fadeInLeft" data-wow-duration="1.5s">
          <h3 class="sec02__itemTitle">相続税のお悩み</h3><img class="sec02__listImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec02_icon-01.png" alt="icon-01">
          <div class="sec02__textListContainer">
            <ul class="sec02__textList">
              <li class="sec02__textItem">・どのくらいの税額？</li>
              <li class="sec02__textItem">・売却したい</li>
              <li class="sec02__textItem">・料金について</li>
            </ul>
          </div>
        </li>
        <li class="sec02__listItem wow animate__animated animate__fadeInUp" data-wow-duration="1.5s">
          <h3 class="sec02__itemTitle">顧問契約</h3>
          <img class="sec02__listImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec02_icon-02.png" alt="icon-02">
          <div class="sec02__textListContainer">
            <ul class="sec02__textList">
              <li class="sec02__textItem">・個人→法人化する</li>
              <li class="sec02__textItem">・経営に関する税務をサポート</li>
            </ul>
          </div>
        </li>
        <li class="sec02__listItem wow animate__animated animate__fadeInRight" data-wow-duration="1.5s">
          <h3 class="sec02__itemTitle">よくあるご質問</h3>
          <img class="sec02__listImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec02_icon-03.png" alt="icon-03">
          <div class="sec02__textListContainer">
            <ul class="sec02__textList">
              <li class="sec02__textItem">・原稿はダミーです。</li>
              <li class="sec02__textItem">・ダミーですダミーです。</li>
              <li class="sec02__textItem">・ダミーです。</li>
            </ul>
          </div>
        </li>
      </ul>
    </section>
    
    
    <section class="sec03">
      <h1 class="sec03__title wow animate__animated animate__fadeInUp" data-wow-duration="1s">豊富な法律知識と実績から 最善の解決方法を ご提案します</h1>
      <p class="sec03__description wow animate__animated animate__fadeInUp" data-wow-duration="1s">税理士は各種税金・税務申告に関する問題をはじめ、<br class="sec03__sp">
        会社の経理や経営相談、相続に代表されるご家族の悩みなどを相談できる専門家です。<br class="sec03__sp">
        相続問題、独立・起業をお考えの方へのサポート、法人・個人事業主様の税務申告・<br class="sec03__sp">
        会計業務と経営支援を業務の中心として取り組んでおります。<br class="sec03__sp">
        フットワーク軽く、一緒に考え、一緒に悩み、皆様の不安を解決して参ります。<br class="sec03__sp">
        一人で悩まず、まずはご相談下さい。
      </p>
    </section>
    
    
    <section class="sec04">
      <img class="sec04__img wow animate__animated animate__fadeInLeft u-pcDb" data-wow-duration="2s" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec04_bg-pc.png" alt="two people meeting">
      <img class="sec04__img wow animate__animated animate__fadeInLeft u-spDb" data-wow-duration="2s" src="<?php echo get_template_directory_uri(); ?>/_assets/images/topPage/topPageSec04_bg-sp.png" alt="two people meeting">
    </section>
    
    
    <section class="sec05">
      <div class="sec05__news">
        <h1 class="sec05__newsTitle wow animate__animated animate__fadeInUp" data-wow-duration="1s">NEWS</h1>
        <ul class="sec05__newsList">
          <li class="sec05__newsItem wow animate__animated animate__fadeInRight" data-wow-duration="1s" data-wow-delay=".2s">2016.05.20　原稿はダミーです。テキストテキストテキストテキストテキストテキストテキストテキスト。</li>
          <li class="sec05__newsItem wow animate__animated animate__fadeInRight" data-wow-duration="1s" data-wow-delay=".3s">2016.05.20　テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ原稿はダミーです。</li>
          <li class="sec05__newsItem wow animate__animated animate__fadeInRight" data-wow-duration="1s" data-wow-delay=".4s">2016.05.20　テキストテキストテキストテキストテキストテキストテキ原稿はダミーです。</li>
        </ul>
        <button class="sec05__btn button-secondary wow animate__animated animate__zoomIn" data-wow-duration="1s">もっと見る<img class="button-secondaryIcon" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/arrow-right_icon.png" alt="arrow"></button>
      </div>
    </section>


    <section class="sec06">
      <h2 class="sec06__title wow animate__animated animate__fadeInUp" data-wow-duration="1s">column</h2>
      <ul class="sec06__columnList">
        <?php 
          $myquery = new WP_Query( array(
            'category_name' => 'column-items',
            'posts_per_page' => -1
          ));
        ?>
        <?php while ( $myquery->have_posts() ) : $myquery->the_post(); ?>

          <li class="sec06__columnItem wow animate__animated animate__fadeInUp" data-wow-duration="1s" data-wow-delay=".1s">
            <a class="sec06__columnLink" href="<?php the_permalink(); ?>"><img class="sec06__columnItemImg" src="<?php the_post_thumbnail_url(null, 'full'); ?>" alt="table">
              <div class="sec06__columnDescription"> 
                <p class="sec06__columnDate"><?php echo get_post_meta($post->ID, 'date', true) ?></p>
                <p class="sec06__columnText"><?php echo get_post_meta($post->ID, 'caption', true) ?></p>
              </div></a>
          </li>

        <?php endwhile; ?>
      </ul>
      
      <button class="sec06__btn button-secondary wow animate__animated animate__zoomIn" data-wow-duration="1s">
        もっと見る<img class="button-secondaryIcon" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/arrow-right_icon.png" alt="arrow"></button>
    </section>
    
    
    <section class="sec07"> 
      <h2 class="sec07__title wow animate__animated animate__fadeInUp" data-wow-duration="1s">豊富な法律知識と実績から最善の解決方法をご提案します</h2>
      <p class="sec07__text wow animate__animated animate__fadeInUp" data-wow-duration="1s">税理士は各種税金・税務申告に関する問題をはじめ、<br class="sec07__br">
        会社の経理や経営相談、相続に代表されるご家族の悩みなどを相談できる専門家です。<br class="sec07__br">
        相続問題、独立・起業をお考えの方へのサポート、法人・個人事業主様の税務申告・<br class="sec07__br">
        会計業務と経営支援を業務の中心として取り組んでおります。<br class="sec07__br">
        フットワーク軽く、一緒に考え、一緒に悩み、皆様の不安を解決して参ります。<br class="sec07__br"> 
        一人で悩まず、まずはご相談下さい
      </p>
      <a href="<?php echo home_url(); ?>/contact-us" class="sec07__contactBtn wow animate__animated animate__pulse" data-wow-duration="1s" data-wow-iteration="infinite">
        お問い合わせはこちら
        <img class="sec07__btnImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/arrow-right_icon.png" alt="arrow"></a>
    </section>
    
    
    <section class="sec08"> 
      <h3 class="sec08__text">アクセス</h3>
      <div class="sec08_mapContainer wow animate__animated animate__zoomIn" data-wow-duration="1.5s">
        <iframe class="sec08__map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6479.87846238604!2d139.580161!3d35.703113!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd46a7a747391afaf!2z5ZCJ56Wl5a-66aeF!5e0!3m2!1sja!2sph!4v1617862231273!5m2!1sja!2sph" width="883" height="440" allowfullscreen="" loading="lazy"> </iframe>
      </div>
      <p class="sec08__description">JR線「吉祥寺駅」から徒歩7分</p>
    </section>

<?php get_footer(); ?>