<?php
$postType = $post->post_type; // Post Type
$postTypeName = get_post_type_object($postType)->label; // CPT Name
$postID = $post->ID; // Post ID
$postTitle = $post->post_title; // Post Title
$postTerms_objArray = get_the_terms($postID, 'taxonomyName'); // Post's term's object's array
$prevPost_obj = get_previous_post(); // Prev Post Object
$nextPost_obj = get_next_post(); // Next Post Object



get_header(); ?>
<div class="wrapper post-container">
    <h1><?php echo $postTitle; ?></h1>
    <?php
    if ( locate_template( '_inc/single/single__' . $postType . '.php' ) ): // If there is incrude file
        include locate_template( '_inc/single/single__' . $postType . '.php' );

    else:
        if ( have_posts() ) :
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
        endif;
    endif;
    ?>

    <div class="post-pagination">

        <div class="pagination-prev">
        <?php if ( is_a( $prevPost_obj , 'WP_Post' ) ) : ?>
            <?php $prevPost_link = get_permalink($prevPost_obj->ID); // Prev Post URL ?>
            <a href="<?php echo $prevPost_link ?>">« 前を見る</a>
        <?php endif; ?>
        </div>
        
        <div class="pagination-next">
        <?php if ( is_a( $nextPost_obj , 'WP_Post' ) ) : ?>
            <?php $nextPost_link = get_permalink($nextPost_obj->ID); // Next Post URL ?>
            <a href="<?php echo $nextPost_link ?>">次を見る »</a>
        <?php endif; ?>
        </div>
        
    </div>


</div>

<?php
get_footer();
?>
