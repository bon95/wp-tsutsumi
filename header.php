
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, user-scalable=1, initial-scale=1, viewport-fit=cover">
    <meta name="keywords" content="Tsutsumi">
    <meta name="description" content="Tsutsumi">
    <meta property="og:title" content="Coding Trial (Tsutsumi)">
    <meta property="og:type" content="website">
    <meta property="og:description" content="Tsutsumi">
    <meta property="og:url" content="http://website.com">
    <meta property="og:image" content="./_assets/images/_etc/ogp.jpg">
    <meta property="fb:app_id" content="XXXXXXXXXXXXXXXX">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="@XXXXX">
    <meta name="twitter:creator" content="@XXXXX">
    <meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/_assets/images/_etc/ogp.jpg">
    <link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/_assets/images/_etc/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/_assets/images/_etc/favicon.png">
    <link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/_assets/images/_etc/custom-icon.png">
    
    <!-- wp_head START -->
    <?php wp_head(); ?>
    <!-- wp_head END -->
</head>

<body class="topPage">
  <?php
    function isCurrent($x) {
      global $post;
      $post_slugMain = $post->post_name;
      $parentobj = get_post($post->post_parent);
      $parentslug = $parentobj->post_name;

      if ( $post_slugMain == $x || $parentslug == $x ): echo "siteHeader__is-current"; endif;
    }
  ?>

    <header class="siteHeader">
      <div class="siteHeader__inner">
        <div class="siteHeader__burgerBtn" id="burger-btn">
          <span class="siteHeader__line"></span>
          <span class="siteHeader__line"></span>
          <span class="siteHeader__line"></span>
        </div>
        <a href="<?php echo home_url(); ?>" class="siteHeader__logoContainer">
          <?php
            if(function_exists('the_custom_logo')) {
              $custom_logo_id = get_theme_mod('custom_logo');
              $logo = wp_get_attachment_image_src($custom_logo_id);
            }
          ?>
          <img class="siteHeader__logo" src="<?php echo $logo[0] ?>" alt="tsutsumi logo">
        </a>
        <div class="siteHeader__contactContainer">
          <img class="siteHeader__img" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/siteHeader_contact.png" alt="contact">
          <div class="siteHeader__contactBtn wow animate__animated animate__zoomIn" data-wow-duration="1.5s">
            <a href="<?php echo home_url(); ?>/contact-us" class="siteHeader__contactBtnText">まずは無料相談・お問い合わせ</a>
            <img class="siteHeader__contactBtnImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/arrow-right_icon.png" alt="arrow">
          </div>
        </div>
        <?php wp_nav_menu(
              array(
                'theme_location' => 'header-menu',
                'container_class' => 'siteHeader__nav',
                'menu_class' => 'siteHeader__navList',
                'add_li_class' => 'siteHeader__navItem',
                'add_a_class' => 'siteHeader__navItemLink',
              )
            ); ?>
      
      </div>
    </header>
    <?php
      if(! is_front_page() && is_page()) { ?>
        <section class="faqSec01">
          <div class="faqSec01__container">
            <h1 class="faqSec01__title"><?php the_title(); ?></h1>
          </div>
        </section>
      <?php }
    ?>