    <footer class="siteFooter">
      <div class="siteFooter__innerCont">
        <div class="siteFooter__toTop js-toTop"><img class="siteFooter__toTopImg" src="<?php echo get_template_directory_uri(); ?>/_assets/images/common/arrow-up_icon.png" alt="arrow up"></div>
        <nav class="siteFooter__nav">
          <ul class="siteFooter__list">
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./.">相続のご相談</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">顧問税理士をお探しの方</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">ご依頼の流れ</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="<?php echo home_url(); ?>/faq">よくあるご質問</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">事務所案内</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">お問い合わせ</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">プライバシーポリシー</a></li>
            <li class="siteFooter__item"><a class="siteFooter__itemLink" href="./#">コラム</a></li>
          </ul>
        </nav>
        <?php
          if(function_exists('the_custom_logo')) {
            $custom_logo_id = get_theme_mod('custom_logo');
            $logo = wp_get_attachment_image_src($custom_logo_id);
          }
        ?>
        <img class="siteFooter__logo" src="<?php echo $logo[0] ?>" alt="">
        <p class="siteFooter__contact">〒180-0002　東京都武蔵野市吉祥寺東町1丁目1番18号<br>TEL 0422-21-3611</p>
      </div>
    </footer>
    <!-- Scripts-->
    <?php wp_footer(); ?>
  </body>
</html>